;; Changing the name of the database tables

RENAME TABLE simple_currency TO currency;
RENAME TABLE simple_currency_history TO currency_history;
RENAME TABLE simple_purchased TO currency_purchased;

;; NOTE: If you changed your DB tables from simple_curency names to
;; base_currency names you will need to change them as follows.
RENAME TABLE base_currency TO currency;
RENAME TABLE base_currency_history TO currency_history;
RENAME TABLE base_purchased TO currency_purchased;

;; NOTE: users of WhiteCore do not need to make this change to be
;; compatible with Virtual Universe